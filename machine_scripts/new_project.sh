# Read the variables
read -p "Nombre de la carpeta: " projectName
read -p "Versión de Magento a instalar (formato x.x.x): " magentoVersion

# Set some variables for the instalation
export MA_BACKEND_FRONTNAME=ma_$projectName
export MA_DB_HOST=127.0.0.1
export MA_DB_NAME=$projectName
export MA_DB_USER=root
export MA_DB_PASSWORD=123456
export MA_USER=vagrant
export MA_PASSWORD=password123
export MA_EMAIL=vagrant@admin.com
export MA_FIRSTNAME=Adminerto
export MA_LASTNAME=López

# Create an empty project
mkdir -p ~/$projectName/
cd ~/$projectName/
composer create-project --repository=https://repo.magento.com/ magento/project-community-edition=$magentoVersion .

# Create the symlink for it to be accesible
cd /var/www/html
sudo ln -s ~/$projectName $projectName 

# Create the database should it not exist
mysql -u root -p123456 -e "create database if not exists ${MA_DB_NAME};"

# Install the Magento
cd ~/$projectName/
rm -rf vendor/* &&
sudo chmod -R 777 . &&
sudo chown -R www-data:www-data . &&
composer install &&
php -dmemory_limit=512M bin/magento setup:install --backend-frontname=$MA_BACKEND_FRONTNAME --db-host=$MA_DB_HOST --db-name=$MA_DB_NAME --db-user=$MA_DB_USER --db-password=$MA_DB_PASSWORD --admin-user=$MA_USER --admin-password=$MA_PASSWORD --admin-email=$MA_EMAIL --admin-firstname=$MA_FIRSTNAME --admin-lastname=$MA_LASTNAME &&
php -dmemory_limit=512M bin/magento setup:upgrade &&
php -dmemory_limit=512M bin/magento setup:di:compile
php -dmemory_limit=512M bin/magento deploy:mode:set developer
sudo chmod -R 777 . && sudo chown -R www-data:www-data .
rm -rf vendor/*
composer install

echo "\n\nDone!"
