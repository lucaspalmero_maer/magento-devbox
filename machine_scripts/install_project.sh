git clone https://gitlab.com/maer-aws/$1.git ~/$1  &&
cd /var/www/html &&
sudo ln -s ~/$1 $1 &&
cd ~/$1/scripts &&
chmod +x deploy.vagrant.sh &&
chmod +x deploy.sh &&
bash deploy.vagrant.sh
