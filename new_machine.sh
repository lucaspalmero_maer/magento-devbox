read -p "Nombre de la caja: " projectName
read -p "Versión de PHP a instalar (formato x.x): " phpVersion
read -p "Llave pública de Magento Marketplace: " magentoPublicKey
read -p "Llave privada de Magento Marketplace: " magentoPrivateKey
read -p "IP para instancia (Recomendada: 10.0.0.x): " vagrantIp

mkdir -p boxes/$projectName/magento
cp etc/vagrantfile.original boxes/$projectName/vagrantfile

sed -ie s/###magentoPublicKey###/$magentoPublicKey/g boxes/$projectName/vagrantfile
sed -ie s/###magentoPrivateKey###/$magentoPrivateKey/g boxes/$projectName/vagrantfile
sed -ie s/###vagrantIp###/$vagrantIp/g boxes/$projectName/vagrantfile
sed -ie s/###phpVersion###/$phpVersion/g boxes/$projectName/vagrantfile

cd boxes/$projectName

vagrant up \
&& vagrant ssh -c "(echo \"\"; echo \"\";) | sudo smbpasswd -as vagrant" \
&& vagrant halt \
&& echo "Finalizada la instalación! Para más información, leé el README.md." \
&& read -n 1 -s -r -p "Presioná cualquier tecla para finalizar."
